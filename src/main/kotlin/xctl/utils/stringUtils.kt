package xctl.utils

import strman.Strman

fun String.toCamelCase() = Strman.toCamelCase(this)

fun String.toKebabCase() = Strman.toKebabCase(this)

