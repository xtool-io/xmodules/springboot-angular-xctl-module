package xctl.utils

import org.jboss.forge.roaster.model.source.JavaClassSource
import java.lang.IllegalArgumentException
import java.math.BigDecimal
import java.math.BigInteger
import java.time.LocalDate
import java.time.LocalDateTime

typealias JavaClass = JavaClassSource

fun javaTypeClassOf(type: String) = when (type) {
    "String" -> String::class.java
    "LocalDateTime" -> LocalDateTime::class.java
    "LocalDate" -> LocalDate::class.java
    "Boolean" -> Boolean::class.javaObjectType
    "Long" -> Long::class.javaObjectType
    "Short" -> Short::class.java
    "Integer" -> Integer::class.java
    "BigDecimal" -> BigDecimal::class.java
    "BigInteger" -> BigInteger::class.java
    "Float" -> Float::class.java
    "Double" -> Double::class.java
    else -> throw IllegalArgumentException("Não foi possível identificar o tipo: $type")
}
