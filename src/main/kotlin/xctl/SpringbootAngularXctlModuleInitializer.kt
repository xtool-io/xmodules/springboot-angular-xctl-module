package xctl

import org.fusesource.jansi.Ansi
import org.slf4j.LoggerFactory
import org.springframework.core.env.Environment
import org.springframework.stereotype.Component
import picocli.CommandLine
import picocli.CommandLine.Command
import xctl.core.WorkspaceContext
import xctl.utils.header
import xdep.kinquirer.command.AbstractInitializer
import xdeps.xdepfs.context.CopyFileContext
import xdeps.xdepfs.tasks.FileSystemTask

/**
 * Classe responsável pela criação (inicialização) do projeto SpringBoot e Angular.
 */
@Component
@Command(name = "init")
class SpringbootAngularXctlModuleInitializer(
    private val workspaceContext: WorkspaceContext,
    private val env: Environment,
    private val fsTask: FileSystemTask,
) : AbstractInitializer() {

    private val log = LoggerFactory.getLogger(SpringbootAngularXctlModuleInitializer::class.java)

    @CommandLine.Option(names = ["--description"], description = ["Descrição do projeto"], required = true)
    lateinit var projectDescription: String
    override fun run() {
        header("Copiando arquivos do projeto")
        val copyVars = mapOf(
            "projectName" to workspaceContext.basename,
            "projectDesc" to projectDescription,
            "rootDir" to env.getRequiredProperty("project.rootDir"),
            "rootPackage" to env.getRequiredProperty("project.rootPackage")
        )
        log.debug("Variáveis para o fsTask.copyFiles: {}", copyVars)
        fsTask.copyFiles(
            classpathSource = "archetype",
            destination = workspaceContext.path,
            vars = copyVars,
            onEachCreated = { ctx: CopyFileContext -> log.info("${Ansi.ansi().fgBrightGreen().a("(create)").reset()} ${ctx.path} (${ctx.byteCountToDisplaySize})") }
        )
//        header("Baixando dependências do frontend")
//        npmTask.install()
//        header("Inicializando o projeto no git")
//        gitTask.init()
//        gitTask.add().apply {
//            log.info("${this?.entryCount} arquivos adicionados ao repositório git.")
//        }
//        header("Instruções gerais")
//        echo(
//            """
//          Aplicação @|blue,bold ${workspaceContext.basename}|@ gerada com sucesso!
//
//          -----
//          Procedimentos para execução local
//
//          @|green 1.|@ Iniciando os serviços do OracleXE e Keycloak:
//            ${'$'} @|bold sudo docker-compose up |@ (Em algumas versões do docker o comando pode ser: ${'$'} @|bold sudo docker compose up|@)
//
//          @|green 2.|@ Iniciando o frontend:
//            ${'$'} @|bold npx ng s |@
//
//          @|green 3.|@ Iniciando o backend:
//            ${'$'} @|bold mvn clean spring-boot:run -s .m2/settings.xml|@
//
//          -----
//          Para executar a ferramenta @|blue,bold xctl|@ execute o commando abaixo:
//            ${'$'} @|bold ./xctl.sh |@
//
//          -----
//          Informações adicionais:
//
//          @|bold Aplicação |@ => http://localhost:4200 (root/root)
//          @|bold Keycloak |@  => http://localhost:8085 (admin/admin)
//          @|bold OracleXE |@  => Ver arquivo application.yml
//
//          Para mais informações leia o README.md do projeto.
//
//        """.trimIndent()
//        )

//        shellTask.runCommand(
//            command = """
//                git init &&
//                git add . &&
//                git commit -m "First Commit"
//            """.trimIndent()
//        )
    }
}
