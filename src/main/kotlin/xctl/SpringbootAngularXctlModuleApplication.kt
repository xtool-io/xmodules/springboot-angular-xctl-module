package xctl

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.builder.SpringApplicationBuilder

@SpringBootApplication
class SpringbootAngularXctlModuleApplication

fun main(args: Array<String>) {
    SpringApplicationBuilder(SpringbootAngularXctlModuleApplication::class.java)
        .logStartupInfo(false)
        .headless(false)
        .run(*args)
}
