package xctl.command

import org.springframework.stereotype.Component
import picocli.CommandLine.Command
import xdep.kinquirer.command.AbstractCommand

/**
 * Classe de comando base para os comando **plantuml** .
 */
@Component
@Command(name = "plantuml", subcommands = [PlantGenerateEntityCommand::class])
class PlantCommand : AbstractCommand() {



    override fun run() {
    }
}
