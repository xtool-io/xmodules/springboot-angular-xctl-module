package xctl.command.provider

import org.springframework.core.env.Environment
import org.springframework.stereotype.Component
import xctl.core.WorkspaceContext
import xdeps.xdepplantuml.tasks.PlantTask

@Component
class PlantClassDiagramValueProvider(
    private val workspaceContext: WorkspaceContext,
    private val env: Environment,
    private val plantTask: PlantTask
) : Iterable<String> {
    override fun iterator(): Iterator<String> {
        val plantClassDiagramsPath = workspaceContext.path.resolve(env.getRequiredProperty("project.paths.plantDiagrams"))
        return plantTask.listDiagrams(plantClassDiagramsPath)
            .map { it.name }
            .listIterator()

    }
}
