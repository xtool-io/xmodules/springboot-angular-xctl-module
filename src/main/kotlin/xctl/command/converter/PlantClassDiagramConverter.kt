package xctl.command.converter

import org.springframework.core.env.Environment
import org.springframework.stereotype.Component
import picocli.CommandLine.ITypeConverter
import xctl.core.WorkspaceContext
import xdeps.xdepplantuml.model.PlantClassDiagram
import xdeps.xdepplantuml.tasks.PlantTask
import java.nio.file.Files

@Component
class PlantClassDiagramConverter(
    private val workspaceContext: WorkspaceContext,
    private val env: Environment,
    private val plantTask: PlantTask
) : ITypeConverter<PlantClassDiagram> {
    override fun convert(value: String?): PlantClassDiagram {
        val plantClassDiagramsPath = workspaceContext.path.resolve(env.getRequiredProperty("project.paths.plantDiagrams"))
        return plantTask.listDiagrams(plantClassDiagramsPath)
            .find { it.name == value }!!
    }
}
