package xctl.command

import org.springframework.core.env.Environment
import org.springframework.stereotype.Component
import picocli.CommandLine
import picocli.CommandLine.Command
import xctl.command.converter.PlantClassDiagramConverter
import xctl.command.provider.PlantClassDiagramValueProvider
import xctl.core.WorkspaceContext
import xctl.service.PlantGenerateEntityService
import xctl.utils.toKebabCase
import xdep.kinquirer.command.AbstractSubCommand
import xdep.kinquirer.components.History
import xdep.kinquirer.core.validate
import xdeps.xdepplantuml.model.PlantClassDiagram
import kotlin.io.path.Path

/**
 * Classe de comando com a funcionalidade de geração de entidades JPA a partir do diagrama de classe plantuml.
 */
@Component
@Command(name = "generate-entities")
class PlantGenerateEntityCommand(
    val plantGenerateEntityService: PlantGenerateEntityService,
    val env: Environment,
    val workspaceContext: WorkspaceContext
) : AbstractSubCommand() {

    /**
     * Option para o diagrama de classe
     */
    @CommandLine.Option(
        names = ["--diagram"],
        description = ["Diagrama de classe PlantUML"],
        completionCandidates = PlantClassDiagramValueProvider::class,
        converter = [PlantClassDiagramConverter::class]
    )
    private lateinit var plantClassDiagram: PlantClassDiagram

    override fun run() {
        // Verifica nos relacionamentos OneToMany os que são do tipo associação e composição
        // para lançar o prompt de input para obter o nome do atributo do relacionamento
        plantClassDiagram.relationships
            .filter { it.isAssociation || it.isComposition }
            .filter { listOf("0", "0..1", "1").contains(it.multiplicities.first) }
            .filter { listOf("0..*", "1..*").contains(it.multiplicities.second) }
            .forEach {
                promptInput(
                    id = "${it.firstEntity.name.toKebabCase()}.${it.secondEntity.name.toKebabCase()}.attrName",
//                    hint = "List<${it.secondEntity.name}>",
                    history = History("plant-generate-entity-command"),
                    validation = { str -> validate(str.isNotEmpty(), "Valor obrigatório!") },
                    message = "Digite o nome do relacionamento OneToMany entre ${it.firstEntity.name} e ${it.secondEntity.name}"
                )
            }
        // Verifica nos relacionamentos ManyToMany os que são do tipo associação e composição
        // para lançar o prompt de input para obter o nome do atributo do relacionamento
        plantClassDiagram.relationships
            .filter { it.isAssociation || it.isComposition }
            .filter { listOf("0..*", "1..*").contains(it.multiplicities.first) }
            .filter { listOf("0..*", "1..*").contains(it.multiplicities.second) }
            .forEach {
                promptInput(
                    id = "${it.firstEntity.name.toKebabCase()}.${it.secondEntity.name.toKebabCase()}.attrName",
//                    hint = "List<${it.secondEntity.name}>",
                    history = History("plant-generate-entity-command"),
                    validation = { str -> validate(str.isNotEmpty(), "Valor obrigatório!") },
                    message = "Digite o nome do relacionamento ManyToMany entre ${it.firstEntity.name} e ${it.secondEntity.name}"
                )
            }
        println("Diagrama de classe selecionado: ${plantClassDiagram.name}, ${plantClassDiagram.classes.size} classes encontradas.")
        val domainPath = workspaceContext.path.resolve(env.getRequiredProperty("project.paths.domainPath"))
        plantGenerateEntityService.exec(
            plantClassDiagram,
            domainPath,
            promptResult
        )
    }
}
