package xctl.core

import org.apache.commons.io.FilenameUtils
import org.springframework.stereotype.Component
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

/**
 * Classe com informações do diretório de trabalho da ferramenta xctl.
 */
@Component
class WorkspaceContext(
    /**
     * Retorna o path onde a ferramenta xctl está sendo executada.
     */
    val path: Path = Paths.get(System.getProperty("user.dir")),
    /**
     * Retorna o nome do diretório de execução da ferramenta xctl.
     */
    val basename: String = FilenameUtils.getBaseName(path.toFile().absolutePath)
) {

    /**
     * Retorna true caso o diretório de trabalho esteja vazio.
     */
    val isEmpty: Boolean = Files.newDirectoryStream(this.path).use { it.none() }

    /**
     * Retorna true caso o diretório de trabalho possua o arquivo descritor xtool.yaml
     */
    val hasDescritor: Boolean = Files.exists(this.path.resolve("xtool.yaml"))
}
