package xctl.service

import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import lombok.EqualsAndHashCode
import lombok.Getter
import lombok.NoArgsConstructor
import lombok.Setter
import org.fusesource.jansi.Ansi
import org.hibernate.annotations.BatchSize
import org.jboss.forge.roaster.model.source.FieldSource
import org.jboss.forge.roaster.model.source.JavaClassSource
import org.slf4j.LoggerFactory
import org.springframework.core.env.Environment
import org.springframework.stereotype.Service
import xctl.core.WorkspaceContext
import xctl.utils.JavaClass
import xctl.utils.javaTypeClassOf
import xctl.utils.toCamelCase
import xctl.utils.toKebabCase
import xdeps.xdepfs.context.CopyFileContext
import xdeps.xdepfs.tasks.FileSystemTask
import xdeps.xdepjava.tasks.JavaTask
import xdeps.xdepplantuml.model.PlantClass
import xdeps.xdepplantuml.model.PlantClassDiagram
import xdeps.xdepplantuml.model.PlantField
import xdeps.xdepplantuml.model.PlantRelationship
import xdeps.xdepplantuml.tasks.PlantTask
import java.nio.file.Path
import java.nio.file.Paths
import javax.persistence.*
import javax.validation.constraints.Size
import kotlin.collections.ArrayList
import kotlin.collections.HashSet

typealias JavaField = FieldSource<JavaClassSource>

@Service
class PlantGenerateEntityService(
    private val workspaceContext: WorkspaceContext,
    private val env: Environment,
    private val plantTask: PlantTask,
    private val javaTask: JavaTask,
    private val fsTask: FileSystemTask,
) {

    private val log = LoggerFactory.getLogger(PlantGenerateEntityService::class.java)

    fun exec(plantClassDiagram: PlantClassDiagram, domainPath: Path, options: Map<String, Any>) {
        log.info("Diagrama de classe: {}, {} classes encontradas.", plantClassDiagram.name, plantClassDiagram.classes.size)
        if (!validateRelationships(plantClassDiagram)) return
        if (!validateMultiplicities(plantClassDiagram)) return
        mapPlantClassesToJpaEntities(plantClassDiagram, domainPath, options)
    }

    /**
     * Realiza o mapeamento das classe PlantUML para as entidades JPA correspondentes.
     */
    private fun mapPlantClassesToJpaEntities(plantClassDiagram: PlantClassDiagram, domainPath: Path, options: Map<String, Any>) {
        plantClassDiagram.classes.forEach { plantClass ->
            log.info(" --- Mapeando classe: ${Ansi.ansi().bold().fgMagenta().a("{}").reset()} ---", plantClass.name)
            // Cria o objeto JavaClass a partir da classe do diagrama
            createJavaClassFrom(plantClass).let { javaClass ->
//                log.info("  ${Ansi.ansi().render("@|faint // Annotations da classe ${plantClass.name} |@").reset()}")
                // Adiciona a annotation @Entity à classe
                addClassEntityAnnotation(javaClass)
                // Adiciona as annotations do Lombok à classe
                addClassLombokAnnotations(javaClass)
                // Itera pelos atributos da classe plantUML
                plantClass.attributes.forEach { plantField ->
//                    log.info("  ${Ansi.ansi().render("@|faint // Atributos da classe ${plantClass.name} |@").reset()}")
                    // Adiciona o atributo a classe
                    createField(plantField, javaClass).let { javaField ->
                        log.info("     ${Ansi.ansi().render("@|faint // Annotations do atributo ${plantField.name} |@").reset()}")
                        // Adiciona as annotations ao atributo Id
                        addFieldIdAnnotations(plantField, javaField, javaClass)
                        // Adiciona a annotation @Column
                        addFieldColumnAnnotation(plantField, javaField, javaClass)
                        // Adiciona a annotation @Size
                        addFieldSizeAnnotation(plantField, javaField, javaClass)
                    }
                }

                // Itera sobre os relacionamentos para efetuar o mapeamento JPA.
                plantClassDiagram.relationships
//                    .filter { it.plantClasses.first.name == plantClass.name }
                    .forEach {
                        addOneToManyAssociationOrComposition(it, javaClass, options)
                        addManyToOneAssociation(it, javaClass)
                        addManyToManyAssociation(it, javaClass, options)
                    }
                writeClass(domainPath, javaClass)
            }
        }
    }


    private fun createJavaClassFrom(
        plantClass: PlantClass
    ): JavaClass {
        return javaTask.createClass(plantClass.name, plantClass.packageName ?: "")
    }

    private fun writeClass(domainPath: Path, javaClass: JavaClass) {
        val entityPath = workspaceContext.path.resolve(domainPath).resolve("${javaClass.name}.java")
        javaTask.writeClass(entityPath, javaClass)
        log.info("${Ansi.ansi().fgBrightGreen().a("(create)").reset()} {}", workspaceContext.path.relativize(entityPath))
        generateDatabinds(javaClass)
    }

    private fun generateDatabinds(javaClass: JavaClass) {
        val copyVars = mapOf(
            "rootPackage" to env.getRequiredProperty("project.rootPackage"),
            "domainPath" to env.getRequiredProperty("project.paths.domainPath"),
            "entity" to javaClass,
        )
        fsTask.copyFiles(
            classpathSource = "templates/databind",
            destination = workspaceContext.path,
            vars = copyVars,
            onEachCreated = { ctx: CopyFileContext ->
                log.info(
                    "${
                        Ansi.ansi().fgBrightGreen().a("(create)").reset()
                    } ${workspaceContext.path.relativize(Paths.get(ctx.path))}"
                )
            },
            onEachSkipped = { ctx: CopyFileContext ->
                log.info(
                    "${Ansi.ansi().render("@|faint (skip) {} |@").reset()}", workspaceContext.path.relativize(Paths.get(ctx.path))
                )
            }
        )
    }

    private fun addClassEntityAnnotation(javaClass: JavaClass) {
        javaTask.addClassAnnotation(javaClass, Entity::class.java)
//        log.info("  ${Ansi.ansi().fgYellow().a("@Entity").reset()}")
        log.info("Annotation do JPA adicionada a classe: ${Ansi.ansi().fgYellow().a("@Entity").reset()}")
    }

    // Adiciona as annotation do Lombok à entidade
    private fun addClassLombokAnnotations(javaClass: JavaClass) {
        javaTask.addClassAnnotation(javaClass, Getter::class.java)
        javaTask.addClassAnnotation(javaClass, Setter::class.java)
        javaTask.addClassAnnotation(javaClass, NoArgsConstructor::class.java)
        javaTask.addClassAnnotation(javaClass, EqualsAndHashCode::class.java) { it.setStringValue("of", "id") }
//        log.info("  ${Ansi.ansi().fgYellow().a("@Getter").reset()}")
//        log.info("  ${Ansi.ansi().fgYellow().a("@Setter").reset()}")
//        log.info("  ${Ansi.ansi().fgYellow().a("@NoArgsConstructor").reset()}")
//        log.info("  ${Ansi.ansi().fgYellow().a("@EqualsAndHashCode").reset()}")
        log.info("Annotations do Lombok adicionadas a classe: ${Ansi.ansi().fgYellow().a("@Getter, @Setter, @NoArgsConstructor, @EqualsAndHashCode").reset()}")
    }


    private fun createField(plantField: PlantField, javaClass: JavaClass): JavaField {
        return javaTask.addField(javaClass, plantField.name) {
            it.setPrivate()
            it.setType(javaTypeClassOf(plantField.type))
            //log.info("  ${Ansi.ansi().render("@|faint // Attributos |@").reset()}")
//            log.info("  # ${Ansi.ansi().bold().fgBrightBlue().a("{}").reset()} ({})", plantField.name, it.type)
            log.info("Adicionando atributo a classe: ${Ansi.ansi().bold().fgBrightBlue().a("{}").reset()} ({})", plantField.name, it.type)
        }
    }


    //Adiciona as annotation para o atributo Id
    private fun addFieldIdAnnotations(plantField: PlantField, javaField: JavaField, javaClass: JavaClass) {
        if (plantTask.isId(plantField)) {
            javaTask.addFieldAnnotation(javaField, Id::class.java)
//            log.info("     ${Ansi.ansi().fgYellow().a("@Id").reset()}")
            javaTask.addFieldAnnotation(javaField, GeneratedValue::class.java) {
                it.setEnumValue("strategy", GenerationType.SEQUENCE)
                it.setStringValue("generator", "SEQ_${javaClass.name.take(26).uppercase()}")
//                log.info("     ${Ansi.ansi().fgYellow().a("@GeneratedValue").reset()}(strategy=GenerationType.SEQUENCE, generator={})", it.getStringValue("generator"))
            }
            javaTask.addFieldAnnotation(javaField, SequenceGenerator::class.java) {
                it.setLiteralValue("initialValue", "1")
                it.setLiteralValue("allocationSize", "1")
                it.setStringValue("name", "SEQ_${javaClass.name.take(26).uppercase()}")
//                log.info("     ${Ansi.ansi().fgYellow().a("@SequenceGenerator").reset()}(initialValue=1, allocationSize=1, name={})", it.getStringValue("name"))
            }
//            log.info("Adicionando annotations para atributo identificador: ")
        }
    }

    private fun addFieldColumnAnnotation(plantField: PlantField, javaField: JavaField, javaClass: JavaClass) {
        // Apenas aplica a annotation @Column caso o atributo não seja Id.
        if (!plantTask.isId(plantField)) {
            javaTask.addFieldAnnotation(javaField, Column::class.java) {
                if (plantTask.containsProperty(plantField, "unique")) it.setLiteralValue("unique", "true")
                if (plantTask.containsProperty(plantField, "notnull")) it.setLiteralValue("nullable", "false")
                // Adiciona o atributo length a annotation
                if (plantField.type == "String") {
                    if (plantTask.hasMultiplicity(plantField)) {
                        it.setLiteralValue("length", plantField.multiplicity)
                    }
                }
                if (plantField.type == "Boolean") {
                    it.setStringValue("columnDefinition", "NUMBER(1,0) DEFAULT 0")
                }
                log.info(
                    "     ${Ansi.ansi().fgYellow().a("@Column").reset()}(unique={}, nullable={}, length={})",
                    plantTask.containsProperty(plantField, "unique"),
                    !plantTask.containsProperty(plantField, "notnull"),
                    plantField.multiplicity.let { 255 }
                )
            }
        }
    }

    private fun addFieldSizeAnnotation(plantField: PlantField, javaField: JavaField, javaClass: JavaClass) {
        if (!plantTask.isId(plantField)) {
            if (plantTask.hasMultiplicity(plantField)) {
                javaTask.addFieldAnnotation(javaField, Size::class.java) {
                    it.setLiteralValue("max", plantField.multiplicity)
                    log.info("    @Size(max={})", plantField.multiplicity)
                }
            }
        }
    }

    /**
     * Adiciona uma associação OneToMany
     */
    private fun addOneToManyAssociationOrComposition(plantRelationship: PlantRelationship, javaClass: JavaClass, options: Map<String, Any>) {
        if (plantRelationship.plantClasses.first.name != javaClass.name) return
        if (plantRelationship.isAssociation or plantRelationship.isComposition) {
            // Verifica se o relacionamento da esquerda é do tipo "One"
            if (listOf("0", "0..1", "1").contains(plantRelationship.multiplicities.first)) {
                // Verifica se o relacionamento da direita é do tipo "Many"
                if (listOf("0..*", "1..*").contains(plantRelationship.multiplicities.second)) {
                    val targetEntity = plantRelationship.secondEntity
                    javaClass.addImport(List::class.java)
                    javaClass.addImport(ArrayList::class.java)
                    javaClass.addImport("${javaClass.`package`}.databind.${targetEntity.name}Databind")
                    val attrName = options["${plantRelationship.firstEntity.name.toKebabCase()}.${plantRelationship.secondEntity.name.toKebabCase()}.attrName"] as String
                    javaTask.addField(javaClass, attrName) {
                        it.setPrivate()
                        it.setType("List<${targetEntity.name}>")
                        it.literalInitializer = "new ArrayList<>()"
                        val oneToManyAnn = it.addAnnotation(OneToMany::class.java)
                        if(plantRelationship.isComposition) oneToManyAnn.setEnumValue("cascade", CascadeType.ALL).setLiteralValue("orphanRemoval", "true")
                        it.addAnnotation(BatchSize::class.java).setLiteralValue("size", "10")
                        it.addAnnotation(JsonDeserialize::class.java).setLiteralValue("contentUsing", "${targetEntity.name}Databind.IdDeserializer.class")
                        it.addAnnotation(JsonSerialize::class.java).setLiteralValue("contentUsing", "${targetEntity.name}Databind.IdSerializer.class")
                        // Adiciona a annotation @Size caso a multiplicidade seja 1..*
                        if (plantRelationship.multiplicities.second == "1..*") {
                            it.addAnnotation(Size::class.java).setLiteralValue("min", "1")
                        }
                    }
                    if (plantRelationship.isSelfAssociation) {

                    }
                }
            }
        }

    }

    /**
     * Adiciona uma associação ManyToOne
     */
    private fun addManyToOneAssociation(plantRelationship: PlantRelationship, javaClass: JavaClass) {
        if (plantRelationship.plantClasses.first.name != javaClass.name) return
        if (plantRelationship.isAssociation) {
            // Verifica se o relacionamento da esquerda é do tipo "Many"
            if (listOf("0..*", "1..*").contains(plantRelationship.multiplicities.first)) {
                // Verifica se o relacionamento da direita é do tipo "One"
                if (listOf("0", "0..1", "1").contains(plantRelationship.multiplicities.second)) {
                    val targetEntity = plantRelationship.secondEntity
                    javaClass.addImport("${javaClass.`package`}.databind.${targetEntity.name}Databind")
                    javaTask.addField(javaClass, targetEntity.name.toCamelCase()) {
                        it.setPrivate()
                        it.setType(targetEntity.name)
                        it.addAnnotation(ManyToOne::class.java)
                        it.addAnnotation(JoinColumn::class.java).setStringValue("name", "${targetEntity.name.uppercase().take(27)}_ID")
                        it.addAnnotation(JsonDeserialize::class.java).setLiteralValue("contentUsing", "${targetEntity.name}Databind.IdDeserializer.class")
                        it.addAnnotation(JsonSerialize::class.java).setLiteralValue("contentUsing", "${targetEntity.name}Databind.IdSerializer.class")
                    }
                }
            }
        }

    }

    private fun addManyToManyAssociation(plantRelationship: PlantRelationship, javaClass: JavaClass, options: Map<String, Any>) {
        if (plantRelationship.plantClasses.first.name != javaClass.name) return
        if (plantRelationship.isAssociation) {
            // Verifica se o relacionamento da esquerda é do tipo "Many"
            if (listOf("0..*", "1..*").contains(plantRelationship.multiplicities.first)) {
                // Verifica se o relacionamento da direita é do tipo "One"
                if (listOf("0..*", "1..*").contains(plantRelationship.multiplicities.second)) {
                    val sourceEntity = plantRelationship.firstEntity
                    val targetEntity = plantRelationship.secondEntity
                    javaClass.addImport("${javaClass.`package`}.databind.${targetEntity.name}Databind")
                    javaClass.addImport(Set::class.java)
                    javaClass.addImport(HashSet::class.java)
                    val attrName = options["${plantRelationship.firstEntity.name.toKebabCase()}.${plantRelationship.secondEntity.name.toKebabCase()}.attrName"] as String
                    javaTask.addField(javaClass, attrName) {
                        it.setPrivate()
                        it.setType("Set<${targetEntity.name}>")
                        it.literalInitializer = "new HashSet<>()"
                        it.addAnnotation(ManyToMany::class.java)
                        it.addAnnotation(BatchSize::class.java).setLiteralValue("size", "10")
                        val joinTableAnnotation = it.addAnnotation(JoinTable::class.java)
                            .setStringValue("name", "${sourceEntity.name.uppercase().take(15)}_${targetEntity.name.uppercase().take(15)}")
                        joinTableAnnotation.addAnnotationValue("joinColumns", JoinColumn::class.java)
                            .setStringValue("name", "${sourceEntity.name.uppercase().take(27)}_ID")
                        joinTableAnnotation.addAnnotationValue("inverseJoinColumns", JoinColumn::class.java)
                            .setStringValue("name", "${targetEntity.name.uppercase().take(27)}_ID")
                        it.addAnnotation(JsonDeserialize::class.java).setLiteralValue("contentUsing", "${targetEntity.name}Databind.IdDeserializer.class")
                        it.addAnnotation(JsonSerialize::class.java).setLiteralValue("contentUsing", "${targetEntity.name}Databind.IdSerializer.class")
                    }
                }
            }
        }

    }

    private fun validateMultiplicities(plantClassDiagram: PlantClassDiagram): Boolean {
        for (relationship in plantClassDiagram.relationships) {
            if (relationship.isAssociation || relationship.isComposition || relationship.isSelfAssociation) {
                val validMultiplicities = listOf("0", "0..1", "1", "0..*", "1..*")
                if (!validMultiplicities.contains(relationship.multiplicities.first) || !validMultiplicities.contains(relationship.multiplicities.second)) {
                    log.error("* Multiplicidade entre as classes ${relationship.firstEntity.name} e ${relationship.secondEntity.name} inválida.")
                    log.error("  As multiplicidades válidas são: $validMultiplicities")
                    return false
                }
            }
        }
        return true
    }

    /**
     * Validação dos relacionamentos.
     */
    private fun validateRelationships(plantClassDiagram: PlantClassDiagram): Boolean {
        for (relationship in plantClassDiagram.relationships) {
            if (relationship.isAssociation) {
                if (relationship.navigabilities.first and relationship.navigabilities.second) {
                    log.error("* Só são permitidos relacionamentos unidirecionais entre as classes.")
                    log.error(
                        "  Verifique o relacionamento entre {} e {}.",
                        relationship.plantClasses.first.name,
                        relationship.plantClasses.second.name
                    )
                    log.error("----------------")
                    return false
                }
                if (!relationship.navigabilities.first) {
                    log.error("* Por convenção, as associações unidirecionais devem ter a navegabilidade definida da esquerda para a direita.")
                    log.error("  Ajuste o relacionamento para: ${relationship.firstEntity.name} --> ${relationship.secondEntity.name}")
                    log.error("----------------")
                    return false
                }
            }
        }
        return true
    }
}
