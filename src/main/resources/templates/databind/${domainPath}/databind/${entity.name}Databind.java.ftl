package ${rootPackage}.app.domain.databind;

import ${rootPackage}.app.domain.${entity.name};
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.*;

import java.io.IOException;

public class ${entity.name}Databind {
    public static class IdDeserializer extends JsonDeserializer<${entity.name}> {
        @Override
        public ${entity.name} deserialize(JsonParser jp, DeserializationContext deserializationContext) throws IOException {
            JsonNode node = jp.getCodec().readTree(jp);
            if (node.isNumber()) {
                ${entity.name} c = new ${entity.name}();
                c.setId(node.asLong());
                return c;
            } else if (node.isObject()) {
                JsonNode id = node.get("id");
                ${entity.name} c = new ${entity.name}();
                c.setId(id.asLong());
                return c;
            }
            return null;
        }
    }

    public static class IdSerializer extends JsonSerializer<${entity.name}> {
        @Override
        public void serialize(${entity.name} entity, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws
                IOException {
            jsonGenerator.writeNumber(entity.getId());
        }
    }
}


