---
client:
  name: "${projectName}-backend"
  secret: ${r'${keycloak.client.secret}'}
  baseUrl: /${projectName}
  redirectUris: ["/${projectName}/*"]
  serviceAccountsEnabled: true
  authorizationServicesEnabled: true
  standardFlowEnabled: true
  directAccessGrantsEnabled: true
  roles:
    - name: "ADMIN"
      description: Role de Administração

