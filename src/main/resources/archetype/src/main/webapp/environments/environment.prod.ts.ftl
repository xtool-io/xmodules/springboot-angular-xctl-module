export const environment = {
  production: true,
  contextPath: '/${projectName}',
  managementPath: '/${projectName}',
  keycloak: {
    config: {
      url: '/auth',
      clientId: '${projectName}-frontend',
      resourceId: '${projectName}-backend',
      realm: 'TRE-PA'
    },
    initOptions: {
      onLoad: 'login-required',
      checkLoginIframe: true,
      enableLogging: false
    }
  },
  messages: {
    onDeny: 'Você não possui permissões de acesso ao sistema. Entre em contato com o administrador do sistema ou com o ServiceDesk.'
  }
};
