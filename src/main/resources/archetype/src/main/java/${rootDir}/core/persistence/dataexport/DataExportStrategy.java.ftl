package ${rootPackage}.core.persistence.dataexport;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * Interface com o contrato para as estratégias de exportação de dados.
 */
public interface DataExportStrategy {

    DataExportResult exportCsv(List<Object[]> content, DataExportOptions dataExportOptions) throws IOException;
}
