#!/bin/bash
#
# Script de inicialização do módulo springboot-angular-xctl-module
#
set -e

bash <(curl -sSL https://gitlab.com/xtool-io/xmodules/springboot-angular-xctl-module/-/raw/master/src/main/dist/validate-tools.sh)

XTOOL_HOME="$HOME/.xtool/"
XCTLRC_FILE="xctlrc.yaml"
MODULE_ID="46512610"
MODULE_NAME="springboot-angular-xctl-module"
MODULE_RELEASE_URL="https://gitlab.com/api/v4/projects/$MODULE_ID/releases/"
MODULE_PACKAGE_URL="https://gitlab.com/api/v4/projects/$MODULE_ID/packages/generic/$MODULE_NAME"
MODULE_REMOTE_VERSION=$(curl -s $MODULE_RELEASE_URL | jq '.[]' | jq -r '.name' | head -1 | cut -d' ' -f2)
MODULE_VERSION=${1:-$MODULE_REMOTE_VERSION}
# COLOR
BOLD="\033[1m"
CYAN_COLOR="\033[96m"
RED_COLOR="\033[91m"
RESET_COLOR="\033[0m"

WORKSPACE=$(pwd)
PROJECT_NAME=$(basename "$WORKSPACE")

if [[ -n "$(find "$WORKSPACE" -mindepth 1 -maxdepth 1 -type f -o -type d -print -quit)" ]]; then
  echo -e "${RED_COLOR}[ERROR]${RESET_COLOR} Para executar a inicialização do módulo $MODULE_NAME o diretório atual deve estar vazio."
  exit 1
fi

# Verifica se o módulo xctl existe localmente no diretório ~/.xtool/modules
# Caso não existe tenta fazer o download do gitlab releases.
if [[ ! -d "$XTOOL_HOME"/modules/$MODULE_NAME/$MODULE_VERSION ]]; then
  curl -# -0 $MODULE_PACKAGE_URL/"$MODULE_VERSION"/"$MODULE_NAME".zip -o /tmp/"$MODULE_NAME".zip
  mkdir -p "$XTOOL_HOME"/modules/$MODULE_NAME/"$MODULE_VERSION"
  unzip -q /tmp/"$MODULE_NAME".zip -d "$XTOOL_HOME"/modules
fi

#
# Copia o xctl.sh e xctlrc.yaml para o workspace
#
cp "$XTOOL_HOME"/modules/$MODULE_NAME/"$MODULE_VERSION"/xctl.sh "$WORKSPACE"
cp "$XTOOL_HOME"/modules/$MODULE_NAME/"$MODULE_VERSION"/xctlrc.yaml "$WORKSPACE"

#
# Exibe um prompt para o usuário para inserir a descrição do projeto
echo -e "${CYAN_COLOR}[INFO ]${RESET_COLOR} Módulo ${CYAN_COLOR}${BOLD}$MODULE_NAME@$MODULE_VERSION${RESET_COLOR} carregado."
echo -en "${BOLD}Digite a descrição do projeto:${RESET_COLOR} "
read -r DESCRIPTION

#
# Inicializa a aplicação java no profile init
#
java -jar "$XTOOL_HOME"/modules/$MODULE_NAME/"$MODULE_VERSION"/"$MODULE_NAME"-"$MODULE_VERSION".jar \
  --spring.config.location=file:"$WORKSPACE"/"$XCTLRC_FILE" \
  --spring.main.banner-mode=off \
  --spring.profiles.active=init \
  --spring.application.name="$PROJECT_NAME" \
  --description "$DESCRIPTION"

chmod +x "$WORKSPACE"/xctl.sh

#
# Roda o npm i
#
npm i

#
# Inicialização do projeto no git
#
#GIT_USER_NAME=$(git config user.name)
#GIT_USER_EMAIL=$(git config user.email)
#
##
## Verifica se a variável user.name está definida. Em caso negativo um prompt para usuário
## é exibido para obter o valor da variável.
##
#if [[ -z "$GIT_USER_NAME" ]]; then
#  echo -en "${BOLD}Digite a propriedade user.name do git:${RESET_COLOR} "
#  read -r GIT_USER_NAME
#  git config --global user.name "$GIT_USER_NAME"
#fi
#
##
## Verifica se a variável user.email está definida. Em caso negativo um prompt para usuário
## é exibido para obter o valor da variável.
##
#if [[ -z "$GIT_USER_EMAIL" ]]; then
#  echo -en "${BOLD}Digite a propriedade user.name do git:${RESET_COLOR} "
#  read -r GIT_USER_EMAIL
#  git config --global user.email "$GIT_USER_EMAIL"
#fi

git init -q
git add .
git commit -m "First Commit" >/dev/null

#
# Informações gerais
#
echo -e ""
echo -e "Aplicação ${CYAN_COLOR}${BOLD}$PROJECT_NAME${RESET_COLOR} gerada com sucesso!"
echo -e ""
echo -e "Leia o ${CYAN_COLOR}${BOLD}README.md${RESET_COLOR} da aplicação para ver as instruções para execução da aplicações em ambiente local."
