#!/bin/bash
#
# Script de execução da ferramenta xctl
#
bash <(curl -sSL https://gitlab.com/xtool-io/xmodules/springboot-angular-xctl-module/-/raw/master/src/main/dist/validate-tools.sh)

XCTLRC_FILE="xctlrc.yaml"
XTOOL_HOME="$HOME"/.xtool
MODULE_VERSION=$(yq '.module.version' < "$XCTLRC_FILE")
PROJECT_ID=$(yq '.module.gitlabProjectId' < "$XCTLRC_FILE")
PROJECT_NAME=$(yq '.module.name' < "$XCTLRC_FILE")
PROJECT_PACKAGE_URL="https://gitlab.com/api/v4/projects/$PROJECT_ID/packages/generic/$PROJECT_NAME"

if [[ ! -d "$XTOOL_HOME"/modules/$PROJECT_NAME/$MODULE_VERSION ]]; then
  curl -# -0 "$PROJECT_PACKAGE_URL"/"$MODULE_VERSION"/"$PROJECT_NAME".zip -o /tmp/"$PROJECT_NAME".zip
  mkdir -p "$XTOOL_HOME"/modules/"$PROJECT_NAME"/"$MODULE_VERSION"
  unzip -q /tmp/"$PROJECT_NAME".zip -d "$XTOOL_HOME"/modules
fi

java -jar "$XTOOL_HOME"/modules/"$PROJECT_NAME"/"$MODULE_VERSION"/"$PROJECT_NAME"-"$MODULE_VERSION".jar \
    --spring.config.location=file:"$(pwd)"/$XCTLRC_FILE



