#!/bin/bash
#
# Script com as validações para rodar a ferramenta
#
JAVA_VERSION="1.8"
NODE_VERSION="12.13.0"
# COLOR
BOLD="\033[1m"
CYAN_COLOR="\033[96m"
RED_COLOR="\033[91m"
RESET_COLOR="\033[0m"

if ! command -v unzip >/dev/null; then
  echo "Ferramenta unzip não encontrada."
  echo "======================================================================================================"
  echo " Por favor, instale a ferramenta unzip com o seu gerenciador de pacotes "
  echo ""
  echo " Após a instalação repita a operação."
  echo "======================================================================================================"
  echo ""
  exit 1
fi

if ! command -v zip >/dev/null; then
  echo "Ferramenta zip não encontrada."
  echo "======================================================================================================"
  echo " Por favor, instale a ferramenta zip com o seu gerenciador de pacotes "
  echo ""
  echo " Após a instalação repita a operação."
  echo "======================================================================================================"
  echo ""
  exit 1
fi

if ! command -v yq >/dev/null; then
  echo "Ferramenta yq não encontrada."
  echo "======================================================================================================"
  echo " Por favor, instale a ferramenta yq com os procedimentos encontrado no endereço abaixo: "
  echo " MacOS: brew install yq"
  echo " Linux: wget https://github.com/mikefarah/yq/releases/download/v4.31.1/yq_linux_amd64.tar.gz -O - | tar xz && sudo mv yq_linux_amd64 /usr/bin/yq"
  echo ""
  echo " Após a instalação repita a operação."
  echo "======================================================================================================"
  echo ""
  exit 1
fi

if ! command -v jq >/dev/null; then
  echo "Ferramenta jq não encontrada."
  echo "======================================================================================================"
  echo " Por favor, instale a ferramenta jq com o seu gerenciador de pacotes."
  echo ""
  echo " Após a instalação repita a operação."
  echo "======================================================================================================"
  echo ""
  exit 1
fi

if ! command -v git >/dev/null; then
  echo "Ferramenta git não encontrada."
  echo "======================================================================================================"
  echo " Por favor, instale a ferramenta unzip com o seu gerenciador de pacotes "
  echo ""
  echo " Após a instalação repita a operação."
  echo "======================================================================================================"
  echo ""
  exit 1
fi

if [[ ! -d "$HOME"/.sdkman ]]; then
  echo "Ferramenta sdk não encontrada."
  echo "======================================================================================================"
  echo " Por favor, instale o java com o utilitário sdk (https://sdkman.io/install): "
  echo ""
  echo " $ curl -s "https://get.sdkman.io" | bash"
  echo ""
  echo " Após a instalação repita a operação."
  echo "======================================================================================================"
  echo ""
  exit 1
fi

if ! command -v java >/dev/null; then
  echo "Ferramenta java não encontrada."
  echo "======================================================================================================"
  echo " Por favor, instale o java com o utilitário sdk (https://sdkman.io/): "
  echo ""
  echo " $ sdk install java 8.0.302-open"
  echo ""
  echo " Após a instalação repita a operação."
  echo "======================================================================================================"
  echo ""
  exit 1
fi

if [[ "$(java -version 2>&1 | awk -F '"' '/version/ {print $2}')" != "$JAVA_VERSION"* ]]; then
  echo "======================================================================================================"
  echo "Para o correto funcionamento do projeto é necessário a versão 1.8 da ferramenta java."
  echo ""
  echo "$ sdk use java 8.0.302-open"
  echo ""
  echo " Após a instalação repita a operação."
  echo "======================================================================================================"
  exit 1
fi

if ! command -v mvn >/dev/null; then
  echo "Ferramenta maven não encontrada."
  echo "======================================================================================================"
  echo " Por favor, instale a ferramenta maven com o utilitário sdk (https://sdkman.io/): "
  echo ""
  echo " $ sdk install maven"
  echo ""
  echo " Após a instalação repita a operação."
  echo "======================================================================================================"
  echo ""
  exit 1
fi

if [[ ! -d "$HOME"/.nvm ]]; then
  echo "Ferramenta nvm não encontrada."
  echo "======================================================================================================"
  echo " Por favor, instale a ferramenta nvm com o utilitário nvm (https://github.com/nvm-sh/nvm#installing-and-updating): "
  echo ""
  echo " $ curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh | bash"
  echo ""
  echo " Após a instalação repita a operação."
  echo "======================================================================================================"
  echo ""
  exit 1
fi

if ! command -v node >/dev/null; then
  echo "Ferramenta node não encontrada."
  echo "======================================================================================================"
  echo " Por favor, instale a ferramenta node com o utilitário nvm: "
  echo ""
  echo " $ nvm install 12.13.0"
  echo ""
  echo " Após a instalação repita a operação."
  echo "======================================================================================================"
  echo ""
  exit 1
fi

if [[ "$(node -v 2>/dev/null)" != "v$NODE_VERSION" ]]; then
  echo "======================================================================================================"
  echo "Para o correto funcionamento do projeto é necessário a versão 12.13.0 da ferramenta node."
  echo ""
  echo "$ nvm use 12.13.0"
  echo ""
  echo " Após a instalação repita a operação."
  echo "======================================================================================================"
  exit 1
fi
