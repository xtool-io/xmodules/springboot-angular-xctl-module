# springboot-angular-xctl-module

Modulo do xctl com as seguintes funcionalidades:

1. Estrutura de projeto Spring Boot (v) a Angular (v11)
2. Suporte a biblioteca visual DevExtreme Angular

## Procedimentos para criação do projeto

Os procedimentos abaixo devem ser executados para a criação da estrutura de um projeto Spring Boot e Angular.

**Passo 1.** Criar um diretório vazio (*ver exemplo abaixo*)

```shell
$ mkdir ~/git/<<NOME_PROJETO>>
```

> O nome do diretório será o nome do projeto

**Passo 2.** Entrar no diretório recém-criado

```shell
$ cd ~/git/<<NOME_PROJETO>>
```

**Passo 3.** Executar o comando para execução do módulo xctl

```shell
$ bash <(curl -sSL https://gitlab.com/xtool-io/xmodules/springboot-angular-xctl-module/-/raw/master/src/main/dist/initializer.sh)
```

Após a execução com sucesso, seguir as instruções do README.md localizado na raiz do projeto.

## Procedimentos para desenvolvimento local

Os procedimentos a seguir só precisam ser excutados em caso de contribuição para o projeto e é independente do procedimento anterior. 

**Passo 1.** Clonar o projeto

```shell
$ git clone git@gitlab.com:xtool-io/xmodules/springboot-angular-xctl-module.git
```

> Após clonar o projeto realizar as alterações no código fonte com as novas funcionalidades/correções.

**Passo 2.** Alterar a versão do módulo no pom.xml

**Passo 3.** Instalar o módulo localmente

O procedimento tem como objetivo realizar a instalação local do módulo xctl em `~/.xtool/modules`

```shell
$ mvn clean install
```

**Passo 4.** Para inicializar o módulo com a versão definida no pom.xml

```shell
$ bash <(curl -sSL https://gitlab.com/xtool-io/xmodules/springboot-angular-xctl-module/-/raw/master/src/main/dist/initializer.sh) <<VERSION>>
```

**Passo 5.** (Opcional) Ativando o modo debug

```shell
$ export LOG_LEVEL=DEBUG
```

> Para desativar o mode debug `unset LOG_LEVEL`



